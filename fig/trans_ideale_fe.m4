.PS
scale=0.85
cct_init
  s_init(Windings)
  ifdef(`Windings_1',,`sinclude(FWindings.dim)')
  sinclude(CMman.dim)
  ifdef(`Windings_1',,`sinclude(tst.dim)')

define(`vlight',`rgbstring(0.9,0.9,0.9)')
define(`lightgray',`rgbstring(0.7,0.7,0.7)')

[
  Q: box invis ht 1 wid 5/4
  ironwid = Q.wid/4.5

  line thick ironwid/(1bp__)+linethick from Q.nw \
    to Q.ne then to Q.se then to Q.sw then to Q.nw then to Q.n
  line thick ironwid/(1bp__)-linethick outlined vlight \
    from Q.nw to Q.ne then to Q.se then to Q.sw then to Q.nw \
    then to Q.n

  down_
  P: winding(R,ironwid*5/4,ironwid/2,4,ironwid,vlight) at Q.w
    line left ironwid * 2/3 from P.T1
    arrow right to P.T1
    "$i_1$" at last arrow.center + (0, 7pt__)
    line left ironwid * 2/3 from P.T2
    gap(up_ to 2nd last line.end)
    llabel(-,s_box($v_1$),+)
    "$N_1$" at P.e+(1pt__,0) ljust

  up_
  S: winding(L,ironwid*5/4,ironwid/2,4,ironwid,vlight) at Q.e
    line right ironwid * 2/3 from S.T2
    arrow right from S.T2 to last line.end - (3pt__, 0)
    "$i_2$" at last arrow.center + (0, 7pt__)
    line right ironwid * 2/3 from S.T1
    gap(up_ to 2nd last line.end)
    rlabel(-,s_box($v_2$),+)
    "$N_2$" at S.w-(1pt__,0) rjust

  box dashed rad ironwid/4 wid Q.wid ht Q.ht at Q
  arrow right arrowht from 0.5 between Q.nw and Q.n
  "$\phi$" ljust at Here+(0,6pt__)

  move up ironwid/2 from Q.n
  move down ironwid/2 from Q.s
]

.PE
