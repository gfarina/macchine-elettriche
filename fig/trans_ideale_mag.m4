.PS     
cct_init 
elen = 0.8
Origin: Here
S: source(up_ elen);
   llabel(-,N_1 i_1,+)
   line right_ elen*3/2
   source(down_ elen); llabel(+,N_2 i_2,-)
   line to Origin

arrow from S.center + (0, 9pt__) to S.end - (0, 6pt__)
"$\phi$" at Here+(.1,-2pt__)
.PE                            # Pic input ends
