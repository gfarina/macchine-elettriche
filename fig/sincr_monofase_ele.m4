.PS
cct_init

left_
  PIN1T: Here
  line left_ .3
  resistor(left_ .5)
  rlabel(,R_s,)
  move left_ .5 
  inductor(right_ .5,W)
  llabel(,j X_s,)
  move left_ .5
  line left_ .2

down_
  source(down_ .9)
  rlabel(+,`\displaystyle\frac{d\bm{\bar{\Psi}_{r|r}}}{dt}',-)
  Continue: Here
  em_arrows(I, 0) with .w at last [].center + (.18,0)
  "$\displaystyle P_\text{mecc}$" at Here + (.15, .2)
  move to Continue

right_
  line to (PIN1T, Here)
  PIN1B: Here

up_
  gap(from PIN1B to PIN1T)
  clabel(,`\bm{\bar{V}_{s}}',)

  "$+$" at PIN1T + (0,-.1)
  "$-$" at PIN1B + (0,.1)

left_
  arrow left_ .2 from PIN1T
  move left_ .05 "$\bm{\bar{I}_{s}}$" above

# "$X_s = \displaystyle\frac{3}{2}\omega L$" at (PIN1T+PIN1B)/2 + (2.6,-.0)
.PE
