unitsize(.8cm);
usepackage("bm");
usepackage("xcolor");

real R1 = 1.5;
real Rh = .15;
real ARR = .5;

draw(arc((0, 0), R1, 50, 50 + 180), linewidth(6) + gray(.8) + linecap(0));
fill(circle((0, 0), R1), gray(1));
draw(arc((0, 0), R1, 50, 50 + 180), black);
draw(arc((0, 0), R1, 50 + 180, 50 + 360), gray);

for (int angle = 0; angle < 360; angle += 20) {
  pair P1 = rotate(angle) * (R1, 0);
  pair P2 = rotate(angle) * (R1 + ARR * abs(Sin(angle)), 0);

  if (angle > 180) {
    pair temp = P1;
    P1 = P2;
    P2 = temp;
  }

  real grayness = .2;
  if (angle < 50 || angle > 50 + 180) grayness = .8;

  draw(P1 -- P2, gray(grayness), EndArrow(size=3.5));
}

for (int angle = 20; angle < 180; angle += 20) {
  pair P1 = rotate(angle) * (R1, 0);
  pair P2 = rotate(-angle) * (R1, 0);

  pair M1 = (.05, 0) + R1 * (1 - Sin(angle));
  if (angle > 90) M1 = -M1;

  draw(P1{-P1} .. M1 .. {P2}P2, gray(.8));
}

void draw_coil(real angle, pen p = black, string lbl = "") { 
  pair A = rotate(angle) * (R1, 0);
  pair B = -A;
  draw(A -- B, linewidth(.6) + p);
  if (lbl != "") {
    draw((0, 0) -- rotate(90) * (A/2), linewidth(.6) + p, EndArrow(size=4));
    label(scale(.5) * lbl, rotate(90) * (1.3 * A / 2));
  }

  fill(circle(B, Rh), white);
  draw(circle(B, Rh), p);
  fill(circle(B, Rh / 2), p);
  
  fill(circle(A, Rh), white);
  draw(circle(A, Rh), p);
  draw((A - (Rh / 2, 0)) -- (A + (Rh / 2, 0)), p);
  draw((A - (0, Rh / 2)) -- (A + (0, Rh / 2)), p);
}

draw_coil(0, gray, "$\color{gray}{\bm{\bar{B}_1}}$");
draw_coil(50);

label(scale(.5) * "$i, N_1$", (-R1, 0), 3W);
label(scale(.5) * "$N_2$", rotate(50 + 180) * (R1 + .6, -.1));
label(scale(.5) * "$\Omega, \Psi$", (-R1, 1.1 * R1));
//label(scale(.5) * "\color{black!100!white}{$\Psi, \Omega$}", rotate(150) * (1.5 * R1, 0));
//draw((rotate(150) * (1.35 * R1, 0)) -- (rotate(150) * (R1 + .07, 0)), gray);

draw((R1 + 3 * Rh, 0) -- (R1 + 1, 0), dotted);
draw(rotate(50) * (R1 + 3 * Rh, 0) -- rotate(50) * (R1 + 1, 0), dotted);
draw(arc((0, 0), R1 + .8, 0, 50), linewidth(.3) , EndArrow(size=3));
label(scale(.5) * "$\theta$", rotate(50 / 2) * (R1 + 1, 0));
