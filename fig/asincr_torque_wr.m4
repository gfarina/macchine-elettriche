.PS
cct_init

define(`f', `7/$1 * $1 * $1 / (20 * $1 * $1 + $1 + 2)')
w = .7

move to (w, 0)
dot(,0)
"$\omega$" at Here + (.1,.1)
move to (w, 0)
linethick_(.5)
for x = 0 to 2.5 by .01 do {
  if (x != 0) then {
    line to (w*(1-x), 1.2 * f(x)) #colored "gray"
  }
}

move to (0, 1.2*f(1))
dot(,0)
"$T_\text{avv}$" rjust above

move to (w,0)
for x = 0 to -1.3 by -.01 do {
  if (x != 0) then {
    line to (w*(1-x), f(x)) #colored "gray"
  }
}

linethick_(.75)
arrow from (-1.2,0) to (1.7,0)
"$\omega_r$" ljust above
arrow from (0,-.7) to (0,.8)
"$T$" above ljust

"Motore" at (w/2, .15)
"Generatore" at (1.2, -.15)
"Freno" at (-.4, .15)

dot(at (w*(1-.316), 1.2 * f(.316)),.0)
"$T_\text{max}$" above ljust
.PE
