unitsize(.8cm);
usepackage("bm");
usepackage("xcolor");

real R1 = 1.5;
real Rh = .15;
real ARR = .5;

fill(circle((0, 0), R1), gray(1));
draw(circle((0, 0), R1), gray(.8));

for (int angle = 20; angle < 360; angle += 20) {
  if (angle == 180) continue;
  pair P1 = rotate(angle) * (R1 + Rh, 0);
  pair P2 = rotate(angle) * (R1 + Rh + ARR * abs(Sin(angle)), 0);

  if (angle > 180) {
    pair temp = P1;
    P1 = P2;
    P2 = temp;
  }

  real grayness = .6;
  //if (angle == 20 || angle == 20 + 180) grayness = 0;

  draw(rotate(-40) * P1 -- rotate(-40) * P2, gray(grayness), EndArrow(size=3.5));
}

for (int angle = 20; angle < 180; angle += 20) {
  pair P1 = rotate(angle) * (R1 + Rh, 0);
  pair P2 = rotate(-angle) * (R1 + Rh, 0);

  pair M1 = (.05, 0) + R1 * (1 - Sin(angle));
  if (angle > 90) M1 = -M1;

  draw(rotate(-40) * (P1{-P1} .. M1 .. {P2}P2), gray(.6));
}

void draw_coil(real angle, pen p = black, string lbl = "", real l) { 
  pair A = rotate(angle) * (R1, 0);
  pair B = -A;
  draw(A -- B, linewidth(.6) + p);
  if (lbl != "") {
    pair P = rotate(90 + angle) * (l, 0);
    draw((0, 0) -- P, linewidth(.6), EndArrow(size=4));
    label(scale(.45) * lbl, P * 1.3);
  }

  fill(circle(B, Rh), white);
  draw(circle(B, Rh), p);
  fill(circle(B, Rh / 2), p);
  
  fill(circle(A, Rh), white);
  draw(circle(A, Rh), p);
  draw((A - (Rh / 2, 0)) -- (A + (Rh / 2, 0)), p);
  draw((A - (0, Rh / 2)) -- (A + (0, Rh / 2)), p);
}

draw((-R1, 0) -- (-R1, .8), linewidth(.8), arrow = EndArrow(size = 3));
label(scale(.45) * "\color{black}{$F$}", (-R1, 1));
draw((R1, 0) -- (R1, -.8), linewidth(.8) + black, arrow = EndArrow(size = 3));
label(scale(.45) * "\color{black}{$F$}", (R1, -1));

//draw_coil(0, gray, "$\color{gray}{\bm{\bar{B}_1}}$", R1/2);
draw_coil(0, black, "$\color{black}{\bm{\bar{B}}}$ ", R1/2);

//label(scale(.5) * "$i_1, N_1$", (-R1, 0), 2W);
label(scale(.5) * "$i, N$", rotate(180) * (R1 + .4, .3));

draw(rotate(50) * (1.9, 0) -- rotate(50) * (R1 + 1.2, 0), dotted);
draw(rotate(0) * (2.2, 0) -- rotate(0) * (R1 + 1.2, 0), dotted);
draw(arc((0, 0), R1 + 1, 0, 50), linewidth(.3) , EndArrow(size=3));
label(scale(.5) * "$\theta$", rotate(25) * (R1 + 1.2, 0)); 

//draw(rotate(50) * (1.9, 0) -- rotate(50) * (R1 + 1, 0), dotted);
//draw(rotate(90) * (1.3, 0) -- rotate(90) * (R1 + 1, 0), dotted);
//draw(arc((0, 0), R1 + .8, 50, 90), linewidth(.3) , EndArrow(size=3));
//label(scale(.5) * "$\frac{\pi}{2} - \theta$", rotate(70) * (R1 + 1, 0)); 


pair P = rotate(50) * (R1 / 1.1, 0);
draw((0, 0) -- P, linewidth(.6), EndArrow(size=4));
label(scale(.45) * "$\color{black}{\bm{\bar{B}_\textrm{tot}}}$", P * 1.2);

//label(scale(.45) * "$\pi/2 + \theta", (R1 + 2, 0));
