.PS     
cct_init

up_
  dot
  source(up_ .8);
  llabel(-,N_1 i_1,+)
  A: Here  
  dot
  line up_ .2

right_
  resistor(right_ 1.7)
  llabel(,R_\text{Fe},)

down_
  line down_ .2
  dot
  B: Here
  source(down_ .8)
  llabel(+,N_2 i_2,-)
  dot
  line down_ .2

left_
  line left_ 1.7

up_
  line up_ .2

move to A
right_
  line right_ .4
down_
  resistor(down_ .8)
  llabel(,R_a,)
left_
  line left_ .4

move to B
left_
  line left_ .4
down_
  resistor(down_ .8)
  rlabel(,R_a,)
right_
  line right_ .4

move to A - (0, .2)
arrow up_ .15 "~$\phi_1$" ljust

move to B - (0, .2)
arrow up_ .15 "$\phi_2$~~" rjust
.PE
