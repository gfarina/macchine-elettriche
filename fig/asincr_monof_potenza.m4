.PS
scale=1.12
cct_init

PIN1T: Here

#box ht 1.6 wid 2.5 dashed with .sw at PIN1T + (.35, -1.2)

move to PIN1T

right_
  line right_ .3
  resistor(right_ .5)
  llabel(,R_s,)
  inductor(right_ .7,W)
  llabel(,j(X_s-X_m))
  line right_ .1
  T1: Here
  dot
  line right_ .1
  inductor(right_ .7,W)
  llabel(,j(X_r-X_m),)
  resistor(right_ .5)
  llabel(,R_r,)
  line right_ .3
  RIGHT_TOP: Here
down_
  resistor(down_ 1)
  Continue: Here
  rlabel(,"\colorbox{white}{$\displaystyle\frac{1-x}{x}R_r$}",)
  Point_(50)
  move to last [].center;
  linethick_(.5)
  arrow from Here-vec_(.18,0) to Here+vec_(.18,0)
  linethick_(.75)
  em_arrows(I, 0) with .w at last [].center + (.15,0)
  "$\displaystyle P_\text{mecc}$" at Here + (.15, .25)
  move to Continue
left_
  line to (T1, Here)
  T2: Here
  dot
  line to (PIN1T, Here)
  PIN1B: Here

down_
  inductor(from T1 to T2,W)
  llabel(,jX_m,)


gap(from PIN1B to PIN1T)
clabel(,`\bm{\bar{V}_{s}}',)
"$+$" at PIN1T + (0,-.1)
"$-$" at PIN1B + (0,.1)

arrow right_ .2 from PIN1T
"$\bm{\bar{I}_s}$" above

arrow left_ .2 from RIGHT_TOP
"$\bm{\bar{I}_r}$" above

.PE
