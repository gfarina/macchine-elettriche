.PS
cct_init

define(`f', `7/$1 * $1 * $1 / (20 * $1 * $1 + $1 + 2)')

linethick_(.5)
#line from (0, 0) to (.03, 1.2*f(.03)) * 6 dashed colored "gray"

move to (0,0)
for x = 0 to 1.5 by .01 do {
  if (x != 0) then {
    line to (x, 1.2 * f(x)) #colored "gray"
  }
}

line from (1, 1.2 * f(1)) to (1, 0) #colored "gray"
"$1$" at Here - (0, .1)


move to (0,0)
for x = 0 to -1.1 by -.01 do {
  if (x != 0) then {
    line to (x, f(x)) #colored "gray"
  }
}

linethick_(.75)
arrow from (-1.2,0) to (1.7,0)
"$x$" ljust above
"\phantom{$\omega_m$}" ljust above
arrow from (0,-.7) to (0,.8)
"$T$" above ljust

"Motore" at (.5, .15)
"Generatore" at (-.6, -.15)
"Freno" at (1.3, .15)

dot(at (1, 1.2 * f(1)),0)
"$T_\text{avv}$" above ljust

dot(at (0.316, 1.2 * f(.316)),0)
"$T_\text{max}$" above ljust

.PE
