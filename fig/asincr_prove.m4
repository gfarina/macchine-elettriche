.PS
scale=1.12
cct_init

[
PIN1T: Here

right_
  line right_ .4
  resistor(right_ .5)
  llabel(,R_s,)
  T1: Here
  move right_ .3
  T3: Here
  
down_
  move down_ 1

left_
  move to (T3, Here)
  T4: Here
  move to (T1, Here)
  T2: Here
  line to (PIN1T, Here)
  PIN1B: Here

down_
  define(D, .15)
  inductor(from T3-(0,D) to T4+(0,D),W)
  llabel(,jX_0,)
  resistor(from T1-(0,D) to T2+(0,D))
  rlabel(,R_0,)
  line from T1-(0,D) to T3-(0,D)
  line from T4+(0,D) to T2+(0,D)
  dot(at (T1+T3)/2-(0,D))
  move to (T1+T3)/2
  line down_ D
  dot(at (T2+T4)/2+(0,D))
  move to (T2+T4)/2
  line up_ D
  line from T1 to (T1+T3)/2
  line from T2 to (T2+T4)/2


gap(from PIN1B to PIN1T)
clabel(,`\bm{\bar{V}_{sn}}',)
"$+$" at PIN1T + (0,-.1)
"$-$" at PIN1B + (0,.1)

arrow right_ .2 from PIN1T
"$\bm{\bar{I}_{so}}$" above
] with .PIN1T at (0,0)

"(a)" at last [].s - (0,.2)

###################################
# Prova a rotore bloccato

[
PIN2T: Here

right_
  line right_ .4
  resistor(right_ .5)
  llabel(,R_s + R_r^*,)
  inductor(right_ .6,W)
  llabel(,jX_{kr},)
  line right_ .1

down_
  line down_ 1

left_
  line to (PIN2T, Here)
  PIN2B: Here

gap(from PIN2B to PIN2T)
clabel(,`\bm{\bar{V}_{cc}}',)
"$+$" at PIN2T + (0,-.1)
"$-$" at PIN2B + (0,.1)

arrow right_ .2 from PIN2T
"$\bm{\bar{I}_{n}}$" above
] with .PIN2T at (1.9,0)

"(b)" at last [].s - (0,.2)

.PE
