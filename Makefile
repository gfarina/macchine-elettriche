#
# Simple Makefile for LaTeX
#
# Author: Marco Elver <me AT marcoelver.com>
# Version: 20130924-1

DOC_PREFIX=macchine_elettriche
BIB_FILES=
SUPPORT_FILES=fig/giunto_elettromagnetico__asy.pdf fig/ge_mutua_induttanza__asy.pdf fig/ge_coppia__asy.pdf fig/trans_ideale_fe__m4.tex fig/trans_ideale_mag__m4.tex fig/trans_ideale_ele__m4.tex fig/trans_perm_ele__m4.tex fig/trans_disperso_mag__m4.tex fig/trans_perm_mag__m4.tex fig/trans_disperso_ele__m4.tex fig/trans_completo_ele__m4.tex fig/sincr_monofase_ele__m4.tex fig/sincr_vettori__m4.tex fig/asincr_monof_potenza_h__m4.tex fig/asincr_monof_potenza__m4.tex fig/asincr_torque_x__m4.tex fig/asincr_torque_wr__m4.tex fig/asincr_monof_ridotto_rotore__m4.tex fig/asincr_monof_approssimato__m4.tex fig/asincr_prove__m4.tex fig/macchina_cc_ele__m4.tex fig/macchina_cc_ecc_serie__m4.tex fig/macchina_cc_serie_caratt__m4.tex

# Additional source files
TEX_FILES=$(shell find ./tex/ -type f -name "*.tex" 2> /dev/null)

# Alternative template path; set in case default is not found below
TEMPLATE_PATH_ALT=

##
# Setup binaries
LATEX=pdflatex -file-line-error -halt-on-error

# Options for this are: bibtex, bibtex8, biber (recommended for BibLaTeX)
BIBGEN=bibtex

# RULES

.PHONY: all
all: pdf view

.PHONY: info
info:
	@echo "[INFO] LaTeX is using TEXINPUTS = $(TEXINPUTS)"

##
# Generate PDF

.PHONY: pdf
pdf: $(DOC_PREFIX).pdf

$(DOC_PREFIX).pdf: $(DOC_PREFIX).tex $(TEX_FILES) $(SUPPORT_FILES) $(DOC_PREFIX).bbl
	@echo "======================================================"
	$(LATEX) $(DOC_PREFIX).tex

support: $(SUPPORT_FILES)

%__eps.pdf: %.eps
	@echo "======================================================"
	epstopdf --outfile=$@ $<

%__svg.pdf: %.svg
	@echo "======================================================"
	inkscape --export-pdf=$@ $<

%__m4.tex: %.m4
	@echo "======================================================"
	m4 -I/Users/gabriele/workspace/circuit_macros ~/workspace/circuit_macros/pgf.m4 $< | dpic -g > $@

# Extract layers from svg as individual pdfs (useful for e.g. animations with beamer)
%__svg-1.pdf: %.svg
	i=1; \
	outprefix=$@; outprefix=$${outprefix%-*}; \
	inkscape --query-all $< | awk -F"," '/^layer/{print $$1}' | \
	while read layer; do \
		inkscape -C -i "$$layer" -j --export-pdf=$${outprefix}-$$((i++)).pdf $< ; \
	done

%__dot.pdf: %.dot
	@echo "======================================================"
	dot -Tpdf $< > $@
	pdfcrop $@ $@

%__dia.pdf: %.dia
	@echo "======================================================"
	dia -n -e $<.eps $<
	epstopdf --outfile=$@ $<.eps

%__asy.pdf: %.asy
	@echo "======================================================"
	asy -f pdf -o $@ $<

$(DOC_PREFIX).bbl: $(BIB_FILES)
	@echo "======================================================"
	$(LATEX) $(DOC_PREFIX).tex
	@echo "------------------------------------------------------"
	$(BIBGEN) $(DOC_PREFIX)
	@echo "------------------------------------------------------"
	$(LATEX) $(DOC_PREFIX).tex

##
# Helper targets

.PHONY: view
view:
	- @open $(DOC_PREFIX).pdf

.PHONY: clean
clean:
	$(RM) $(DOC_PREFIX).out
	$(RM) $(DOC_PREFIX).log
	$(RM) $(DOC_PREFIX).ps
	$(RM) $(DOC_PREFIX).dvi
	$(RM) $(DOC_PREFIX).aux
	$(RM) $(DOC_PREFIX).bbl
	$(RM) $(DOC_PREFIX).blg
	$(RM) $(DOC_PREFIX).toc
	$(RM) $(DOC_PREFIX).lof
	$(RM) $(DOC_PREFIX).lol
	$(RM) $(DOC_PREFIX).lot
	$(RM) $(DOC_PREFIX).nav
	$(RM) $(DOC_PREFIX).snm
	$(RM) $(DOC_PREFIX).bcf
	$(RM) $(DOC_PREFIX).run.xml

.PHONY: cleanall
cleanall: clean
	$(RM) $(DOC_PREFIX).pdf
	$(RM) *__eps.pdf fig/*__eps.pdf
	$(RM) *__svg*.pdf fig/*__svg*.pdf
	$(RM) *__dot.pdf fig/*__dot.pdf
	$(RM) *__dia.pdf fig/*__dia.pdf *.dia.eps fig/*.dia.eps
	$(RM) *__m4.tex fig/*__m4.tex
