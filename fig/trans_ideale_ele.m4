.PS
  cct_init
  down_
  T: transformer(,,6,W,6)
  line up_ .15 from T.P1 then left_ .5
  line down_ .15 from T.P2 then left_ .5
  
  up_
  gap(from last line.end to 2nd last line.end)
  clabel(-, v_1, +)
 
  arrow right .2 from 3rd last line.end
  move right_ .1 "$i_1$" above
 
  line up_ .15 from T.S1 then right_ .5
  line down_ .15 from T.S2 then right_ .5

  up_
  gap(from last line.end to 2nd last line.end)
  clabel(-, v_2, +)

  arrow right .2 from T.S1 + (0, .15)
  move right_ .1 "$i_2$" above

  "$K$" at (T.P1 + T.S1)/2 + (0, .25)
.PE
