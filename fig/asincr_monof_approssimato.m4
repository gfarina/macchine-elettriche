.PS
scale=1.12
cct_init

PIN1T: Here

#box ht 1.6 wid 2.5 dashed with .sw at PIN1T + (.35, -1.2)

move to PIN1T

right_
  line right_ .5
  T1: Here
  #dot
  line right_ .3
  T3: Here
  #dot
  line right_ .1
  
  inductor(right_ .6,W)
  llabel(,jX_{kr},)
  resistor(right_ .6)
  llabel(,R_s + R_r^*,)
  line right_ .1
  RIGHT_TOP: Here
down_
  resistor(down_ 1)
  Continue: Here
  rlabel(,"\colorbox{white}{$\displaystyle\frac{1-x}{x}R_r^*$}",)
  Point_(50)
  move to last [].center;
  linethick_(.5)
  arrow from Here-vec_(.18,0) to Here+vec_(.18,0)
  linethick_(.75)
  #em_arrows(I, 0) with .w at last [].center + (.15,0)
  #"$\displaystyle P_\text{mecc}$" at Here + (.15, .25)
  move to Continue
left_
  line to (T3, Here)
  T4: Here
  #dot
  line to (T1, Here)
  T2: Here
  #dot
  line to (PIN1T, Here)
  PIN1B: Here

down_
  define(D, .15)
  inductor(from T3-(0,D) to T4+(0,D),W)
  llabel(,jX_0,)
  resistor(from T1-(0,D) to T2+(0,D))
  rlabel(,R_0,)
  line from T1-(0,D) to T3-(0,D)
  line from T4+(0,D) to T2+(0,D)
  dot(at (T1+T3)/2-(0,D))
  dot(at (T1+T3)/2)
  line down_ D
  dot(at (T2+T4)/2+(0,D))
  dot(at (T2+T4)/2)
  line up_ D
  


gap(from PIN1B to PIN1T)
clabel(,`\bm{\bar{V}_{s}}',)
"$+$" at PIN1T + (0,-.1)
"$-$" at PIN1B + (0,.1)

arrow right_ .2 from PIN1T
"$\bm{\bar{I}_s}$" above

.PE
