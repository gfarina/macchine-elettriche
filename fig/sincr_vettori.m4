.PS
cct_init

O: Here

Point_(0)
arrow from O to vec_(.6,0)
"$\bm{\bar{I}_r}$" above
arrow from O to vec_(1.0,0)
"$\displaystyle\bm{\bar{\Psi}_{r|r}}$" ljust

Point_(60)
arrow from O to vec_(.7,0)
"$\bm{\bar{I}_s}$" ljust

Point_(90)
arrow from O to vec_(1.2,0)
"$\displaystyle\frac{d\bm{\bar{\Psi}_{s|r}}}{dt} = \bm{\bar{E}}$" below ljust 

Point_(60)
arrow to Here + vec_(.2,0)
"$R_s \bm{\bar{I}_s}$" above ljust

Point_(150)
arrow to Here + vec_(.9,0)
"$\displaystyle jX_s\bm{\bar{I}_s}$" above ljust

arrow from O to Here
"$\bm{\bar{V}_s}$" below rjust

Point_(30)
arcd(O, .25, 0, 60)
arcd(O, .22, 0, 60)
"$\theta$" at vec_(.35, 0)

Point_((90 + 105)/2)
arcd(O, .25, 90, 110)
arcd(O, .22, 90, 110)
"$\delta$" at vec_(.35, 0)

Point_(75)
arcd(O, .25, 60, 90)
arcd(O, .22, 60, 90)
"$\varphi$" at vec_(.35, 0)
.PE
