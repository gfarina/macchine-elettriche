.PS
  cct_init

  PIN1T: Here
  
right_
  line right_ .2
  inductor(right_ .7,W)
  llabel(,jX_1,)
  dot
  line right_ .4
down_
  T: transformer(down_ .7,,6,W,6) with .P1 at Here
  
left_
  line left_ .4 from T.P2
  dot
  move up_ .7
down_
  inductor(down_ .7,W)
  llabel(,jX_0,)
left_
  line left_ (.7 + .2)
  PIN1B: Here
up_
  gap(from PIN1B to PIN1T)
  clabel(-,v_1,+)

move to T.S1
up_
  line up_ .15 then right .2
right_
  inductor(right_ .7, W)
  PIN2T: Here
  llabel(,jX_2,)

move to T.S2
down_
  line down_ .15 then right_ (.2 + .7)
  PIN2B: Here

up_
  gap(from PIN2B to PIN2T)
  clabel(-,v_2,+)

# Now the arrows...
move to PIN1T
right_
  arrow right_ .2
  move right_ .1 "$i_1$" above

  move to T.S1 + (0, .15)
  arrow right_ .2
  move right_ .1 "$i_2$" above

"$K$" at (T.P1 + T.S1)/2 + (0, .25)
 
.PE
