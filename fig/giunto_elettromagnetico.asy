unitsize(.8cm);

real R1 = 1;
real T1 = .5;
real T2 = 1.2;
real Rh = .15;

fill(circle((0, 0), R1 + T1 + T2), gray(.9));
fill(circle((0, 0), R1 + T1), white);
fill(circle((0, 0), R1), gray(.9));

draw(circle((0, 0), R1), linewidth(.8) + gray(.3));
draw(circle((0, 0), R1 + T1), linewidth(.8) + gray(.3));
draw(circle((0, 0), R1 + T1 + T2), linewidth(.8) + gray(.3));

draw((R1 + T1 + T2, 0) -- (R1 + T1 + T2 + .5, 0), dashed);
label(scale(.6) * "0 rad", (R1 + T1 + T2 + .5, 0), E);
draw((0, R1 + T1 + T2) -- (0, R1 + T1 + T2 + .5), dashed);
label(scale(.6) * "$\pi/2$ rad", (0, R1 + T1 + T2 + .5), N);

fill(circle(-(R1 + T1 + Rh, 0), Rh), white);
draw(circle(-(R1 + T1 + Rh, 0), Rh));
fill(circle(-(R1 + T1 + Rh, 0), Rh / 2));

fill(circle((R1 + T1 + Rh, 0), Rh), white);
draw(circle((R1 + T1 + Rh, 0), Rh));
draw((R1 + T1 + Rh / 2, 0) -- (R1 + T1 + 3 * Rh / 2, 0));
draw((R1 + T1 + Rh, -Rh / 2) -- (R1 + T1 + Rh, Rh / 2));

void draw_field_loop(real angle, pen p = gray(.6)) {
  pair U1 = rotate(angle) * (R1, 0);
  pair U2 = rotate(angle) * (R1 + T1, 0);
  pair L1 = rotate(-angle) * (R1, 0);
  pair L2 = rotate(-angle) * (R1 + T1, 0);

  pair M1 = (.05, 0) + R1 * (1 - Sin(angle));
  pair M2 = R1 + T1 + 5 / 2 * Rh + .8 * (T2 - 5 / 2 * Rh) * Sin(angle);
  if (angle >= 90) {
    M1 = -M1;
    M2 = -M2;
  }

  draw(U1 -- U2, p, MidArrow);
  draw(L2 -- L1, p, MidArrow);

  draw(L1{-L1} .. M1 .. {U1}U1, p);
  draw(L2{L2/2} .. M2 .. {-U2/2}U2, p);
}

draw_field_loop(140);
draw_field_loop(160);
draw_field_loop(120);
draw_field_loop(100);
draw_field_loop(40);
draw_field_loop(20);
draw_field_loop(80);

draw_field_loop(60, white + linewidth(4));
draw_field_loop(60, black + linewidth(.8));

label(scale(.6) * "$\gamma$", rotate(60) * (R1 + T1, 0), 2N + .2W);

draw((0, R1) -- (-R1 - T1 - T2 - .7, R1), linewidth(.3));
draw((0, R1 + T1) -- (-R1 - T1 - T2 - .7, R1 + T1), linewidth(.3));

draw((-R1 - T1 - T2 - .5, R1) -- (-R1 - T1 - T2 - .5, R1 + T1), linewidth(.3), arrow = Arrows());
label(scale(.6) * "$t$", (-R1 - T1 - T2 - .5, R1 + T1 / 2), W);

label(scale(.6) * "$N$", (-R1 - T1 - Rh, 0), 1.5W);

